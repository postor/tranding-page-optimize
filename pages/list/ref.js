import List from '../../comps/list/refs/List'
import DataControl from '../../comps/DataControl'
import Style from '../../comps/Style'
import { useData } from '../../lib/data-hooks'

const NormalListPage = () => {
  let data = useData()
  return (<div>
    <Style />
    <List rows={data} />
    <DataControl />
  </div>)
}

export default NormalListPage