import List from '../../comps/list/refs/List'
import DataControl from '../../comps/DataControl'
import Style from '../../comps/Style'
import { useData } from '../../lib/data-hooks'

const BATCH_SIZE = 50
const arr = new Array(BATCH_SIZE).fill(0)

const NormalListPage = () => {
  let data = useData()
  return (<div>
    <Style />
    <DataControl />
    {arr.map((x, i) => (<List key={i} rows={data} />))}
  </div>)
}

export default NormalListPage