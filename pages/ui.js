import StockChart from '../comps/ui/StockChart'
import DepthChart from '../comps/ui/DepthChart'
import MarketList from '../comps/ui/MarketList'
import TradesList from '../comps/ui/TradesList'
import BookList from '../comps/ui/BookList'
import { useData, useWorking } from '../lib/data-hooks'
import DataControl from '../comps/DataControl'
import { useEffect } from 'react'

const UI = () => {
  let data = useData(), [wrking, setWorking] = useWorking()

  useEffect(() => {
    setTimeout(() => setWorking(true), 1000)
  }, [])
  return (<div className="page-container" style={{
    display: 'grid',
    width: '100vw',
    gridTemplateColumns: `33.3333% 33.3333% 33.3333%`,
    gridTemplateRows: '390px auto',
    gridTemplateAreas: `
    "list1 chart1 chart1"
    "list2 chart3 chart2"
    `
  }}>
    <div>
      <div className="c1title">Market</div>
      <MarketList rows={data} />
    </div>
    <div style={{ gridArea: 'chart1' }}>
      <StockChart data={data} />
    </div>
    <div>
      <div className="c1title">Trades</div>
      <TradesList rows={data} />
    </div>
    <div style={{
      paddingLeft: 5
    }}>
      <div className="c1title">Book</div>
      <BookList rows={data} />
    </div>
    <div style={{
      paddingLeft: 5
    }}>
      <div className="c1title">Depth</div>
      <DepthChart data={data} />
    </div>
    <DataControl />
    <style jsx global>{`
    html,body {margin:0;padding:0;background-color:#222}
    .c1title {font-size:.75rem;line-height:18px;color:#999}
    .page-container>div {position: relative}
    .highcharts-navigator-series {
      display: none;
    }
    .highcharts-background {
      fill: #222;
    }
    .green { color: #539d20 }
    .red { color: #da4348 }
    .gray { color: #999 }
    .c1list { 
      color: #fff;
      width: 100%;
      border: 1px solid #666;
      border-collapse: collapse;
      font-size:.75rem; 
    }
    .c1list tr { 
      background-color: #2c3336; 
      height: 20px; 
      line-height: 20px;
      border-bottom: 1px solid #666; 
    }
    .c1list tr:nth-child(even)  { background-color: #262b2d }
    .c1list thead  { background-color: 1b1b1b; color: #999 }
    
    .c1list td { position: relative  }
    .c1list thead td:after { border-right: 1px solid   }
    `}</style>
  </div>)
}

export default UI