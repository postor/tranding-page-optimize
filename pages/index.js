
import Chart from '../comps/chart/SVGLineChart'
import List from '../comps/list/normal/List'
import DataControl from '../comps/DataControl'
import Style from '../comps/Style'
import { useData } from '../lib/data-hooks'

const Index = () => {
  let data = useData()
  return (<div>
    <Style />
    <DataControl />
    <div className="left">
      <List rows={data} />
    </div>
    <div className="left">
      <List rows={data} />
    </div>
    <div className="left">
      <List rows={data} />
    </div>
    <div className="left">
      <Chart data={data} />
    </div>
    <div className="left">
      <Chart data={data} />
    </div>

  </div>)
}
export default Index