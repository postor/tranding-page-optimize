import Chart from '../../comps/chart/SVGLineChart'
import DataControl from '../../comps/DataControl'
import Style from '../../comps/Style'
import { useData } from '../../lib/data-hooks'


const SVGLineChartPage = () => {
  let data = useData()
  return (<div>
    <Style />
    <Chart data={data} />
    <DataControl />
  </div>)
}

export default SVGLineChartPage