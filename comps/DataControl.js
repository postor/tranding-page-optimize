import { useWorking } from '../lib/data-hooks'

const DataControl = () => {
  let [working, setWorking, speed] = useWorking()
  return (<div>
    <p>working:{working}</p>
    <button onClick={() => working
      ? setWorking(false)
      : setWorking(true)}
    >{working ? 'stop' : 'start'}</button>
    <p>milisecs per flush:{speed}</p>
  </div>)
}

export default DataControl