import {
  LineChart, CartesianGrid, XAxis
  , YAxis, Tooltip, Legend, Line
} from 'recharts'

import { prepareRechart } from '../../lib/data-chart-prepare'

const MyLineChart = ({ data = [] }) => {
  if (!data.length) return null
  return (<LineChart width={730} height={250} data={prepareRechart(data)}
    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
  >
    <CartesianGrid strokeDasharray="3 3" />
    <XAxis dataKey="name" />
    <YAxis domain={[0, 1]} />
    <Tooltip />
    <Legend />
    <Line type="monotone" dataKey="value" stroke="#8884d8" isAnimationActive={false} />
  </LineChart>)
}

export default MyLineChart