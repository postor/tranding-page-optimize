const a1 = 80, k1 = (700 - 100) / 9, a2 = 190, k2 = (5 - 190) / 1
const calcX = x => x * k1 + a1, calcY = y => y * k2 + a2
const SVGLineChart = ({ data = [] }) => {
  let points = data.map(({ value }, i) => [calcX(i), calcY(value)])
  return (<svg className="recharts-surface" width="730" height="250" viewBox="0 0 730 250" version="1.1">
    <defs>
      <clipPath id="recharts1-clip">
        <rect x="80" y="5" height="185" width="620">
        </rect>
      </clipPath>
    </defs>
    <g className="recharts-cartesian-grid">
      <g className="recharts-cartesian-grid-horizontal">
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="80" y1="190" x2="700" y2="190">        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="80" y1="143.75" x2="700" y2="143.75">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="80" y1="97.5" x2="700" y2="97.5">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="80" y1="51.25" x2="700" y2="51.25">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="80" y1="5" x2="700" y2="5">
        </line>
      </g>
      <g className="recharts-cartesian-grid-vertical">
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="80" y1="5" x2="80" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="148.88888888888889" y1="5"
          x2="148.88888888888889" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="217.77777777777777" y1="5"
          x2="217.77777777777777" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="286.66666666666663" y1="5"
          x2="286.66666666666663" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="355.55555555555554" y1="5"
          x2="355.55555555555554" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="424.44444444444446" y1="5"
          x2="424.44444444444446" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="493.3333333333333" y1="5"
          x2="493.3333333333333" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="562.2222222222222" y1="5"
          x2="562.2222222222222" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="631.1111111111111" y1="5"
          x2="631.1111111111111" y2="190">
        </line>
        <line strokeDasharray="3 3" stroke="#ccc" fill="none" x="80" y="5"
          width="620" height="185" offset="0" x1="700" y1="5" x2="700" y2="190">
        </line>
      </g>
    </g>
    <g className="recharts-layer recharts-cartesian-axis recharts-xAxis xAxis">
      <line orientation="bottom" width="620" height="30" type="category" x="80" y="190"
        className="recharts-cartesian-axis-line" stroke="#666" fill="none" x1="80" y1="190"
        x2="700" y2="190">
      </line>
      <g className="recharts-cartesian-axis-ticks">
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190"
            className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="80" y1="196"
            x2="80" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="80" y="198"
            stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value"
            textAnchor="middle">
            <tspan x="80" dy="0.71em">stock0</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190"
            className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none"
            x1="148.88888888888889" y1="196" x2="148.88888888888889" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category"
            x="148.88888888888889" y="198" stroke="none" fill="#666"
            className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="148.88888888888889" dy="0.71em">stock1</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190"
            className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none"
            x1="217.77777777777777" y1="196" x2="217.77777777777777" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category"
            x="217.77777777777777" y="198" stroke="none" fill="#666"
            className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="217.77777777777777" dy="0.71em">stock2</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="286.66666666666663" y1="196" x2="286.66666666666663" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="286.66666666666663" y="198" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="286.66666666666663" dy="0.71em">stock3</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="355.55555555555554" y1="196" x2="355.55555555555554" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="355.55555555555554" y="198" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="355.55555555555554" dy="0.71em">stock4</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="424.44444444444446" y1="196" x2="424.44444444444446" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="424.44444444444446" y="198" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="424.44444444444446" dy="0.71em">stock5</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="493.3333333333333" y1="196" x2="493.3333333333333" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="493.3333333333333" y="198" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="493.3333333333333" dy="0.71em">stock6</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="562.2222222222222" y1="196" x2="562.2222222222222" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="562.2222222222222" y="198" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="562.2222222222222" dy="0.71em">stock7</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="631.1111111111111" y1="196" x2="631.1111111111111" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="631.1111111111111" y="198" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="631.1111111111111" dy="0.71em">stock8</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="bottom" width="620" height="30" type="category" x="80" y="190" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="700" y1="196" x2="700" y2="190">
          </line>
          <text orientation="bottom" width="620" height="30" type="category" x="700" y="198" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="middle">
            <tspan x="700" dy="0.71em">stock9</tspan>
          </text>
        </g>
      </g>
    </g>
    <g className="recharts-layer recharts-cartesian-axis recharts-yAxis yAxis">
      <line orientation="left" width="60" height="185" type="number" x="20" y="5" className="recharts-cartesian-axis-line" stroke="#666" fill="none" x1="80" y1="5" x2="80" y2="190">
      </line>
      <g className="recharts-cartesian-axis-ticks">
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="left" width="60" height="185" type="number" x="20" y="5" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="74" y1="190" x2="80" y2="190">
          </line>
          <text orientation="left" width="60" height="185" type="number" x="72" y="190" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="end">
            <tspan x="72" dy="0.355em">0</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="left" width="60" height="185" type="number" x="20" y="5" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="74" y1="143.75" x2="80" y2="143.75">
          </line>
          <text orientation="left" width="60" height="185" type="number" x="72" y="143.75" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="end">
            <tspan x="72" dy="0.355em">0.25</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="left" width="60" height="185" type="number" x="20" y="5" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="74" y1="97.5" x2="80" y2="97.5">
          </line>
          <text orientation="left" width="60" height="185" type="number" x="72" y="97.5" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="end">
            <tspan x="72" dy="0.355em">0.5</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="left" width="60" height="185" type="number" x="20" y="5" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="74" y1="51.25" x2="80" y2="51.25">
          </line>
          <text orientation="left" width="60" height="185" type="number" x="72" y="51.25" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="end">
            <tspan x="72" dy="0.355em">0.75</tspan>
          </text>
        </g>
        <g className="recharts-layer recharts-cartesian-axis-tick">
          <line orientation="left" width="60" height="185" type="number" x="20" y="5" className="recharts-cartesian-axis-tick-line" stroke="#666" fill="none" x1="74" y1="5" x2="80" y2="5">
          </line>
          <text orientation="left" width="60" height="185" type="number" x="72" y="10.5" stroke="none" fill="#666" className="recharts-text recharts-cartesian-axis-tick-value" textAnchor="end">
            <tspan x="72" dy="0.355em">1</tspan>
          </text>
        </g>
      </g>
    </g>
    <g className="recharts-layer recharts-line">
      <polyline stroke="#8884d8" strokeWidth="1" fill="none"
        width="620" height="185" className="recharts-curve recharts-line-curve" type="monotone"
        points={points.map(x => x.join(',')).join(' ')}
      ></polyline>
      <g className="recharts-layer recharts-line-dots">
        {points.map((x, i) => (<circle key={i} r="3" type="monotone" stroke="#8884d8" strokeWidth="1" fill="#fff"
          width="620" height="185" cx={x[0]} cy={x[1]} className="recharts-dot recharts-line-dot">
        </circle>))}
      </g>
    </g>
  </svg>)
}

export default SVGLineChart