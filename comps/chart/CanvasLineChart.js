import { prepareCanvasJS } from '../../lib/data-chart-prepare'
import { useRef, useEffect } from 'react'
import Head from 'next/head'

const isServer = typeof window == 'undefined'

const MyLineChart = ({ data = [] }) => {
  let canvas = useRef(), chart = useRef()
  useEffect(() => {
    // init
    if (isServer) return

    chart.current = new CanvasJS.Chart(canvas.current, {
      animationEnabled: false,
      theme: "light2",
      title: {
        text: "Simple Line Chart"
      },
      data: [{
        type: "line",
        dataPoints: new Array(10).fill(0).map((y, x) => ({ x, y: 0 }))
      }],
      axisY: {
        maximum: 1,
        minimum: 0,
      },
      axisX: {
        maximum: 10,
        minimum: 0,
      },
    })
    chart.current.render()
  }, [])

  useEffect(() => {
    if (!chart.current) return
    chart.current.options.data[0].dataPoints = prepareCanvasJS(data)
    chart.current.render()
  }, [data])

  return (<>
    <Head>
      <script id="canvasjs" src="https://cdn.bootcdn.net/ajax/libs/canvasjs/1.7.0/canvasjs.min.js"></script>
    </Head>
    <div ref={canvas} style={{
      width: 730,
      height: 250
    }} />
  </>)
}

export default MyLineChart