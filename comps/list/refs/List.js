import { useEffect, useRef } from "react"
const ROW_COUNT = 10, isServer = typeof window == 'undefined', KEYS = ['name', 'value', 'flag']

const List = ({ rows = [] }) => {
  let ul = useRef()
  useEffect(() => {
    if (isServer) return
    for (let i = 0; i < ROW_COUNT; i++) {
      ul.current.appendChild(createLi())
    }
  }, [])
  useEffect(() => {
    if (!rows.length) return
    for (let i = 0; i < ROW_COUNT; i++) {
      let li = ul.current.children[i]
      for (let j = 0; j < 3; j++) {
        li.children[j].innerText = rows[i][KEYS[j]]
      }
    }
  }, [rows])
  return (<ul ref={ul} />)
}

export default List

function createLi() {
  let li = document.createElement('li')
  for (let i = 0; i < 3; i++) {
    let span = document.createElement('span')
    li.appendChild(span)
  }
  return li
}