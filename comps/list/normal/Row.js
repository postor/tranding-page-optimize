const Row = ({ data: { name, value, flag } }) => {
  return (<li>
    <span>{name}</span>
    <span>{value}</span>
    <span>{flag}</span>
  </li>)
}

export default Row