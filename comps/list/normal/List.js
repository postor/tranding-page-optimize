import Row from './Row'

const List = ({ rows = [] }) => {
  return (<ul>
    {rows.map((row, i) => (<Row key={i} data={row} />))}
  </ul>)
}

export default List