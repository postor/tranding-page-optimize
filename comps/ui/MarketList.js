const ccys = [
  'BTC/USD',
  'BTC/GBP',
  'BTC/EUR',
  'BTC/JPY',
  'ETH/UST',
  'ETH/EUR',
  'ETH/JPY',
  'ETH/GBP',
  'ETH/USD',
  'ETH/BTC',
]
const Row = ({
  data = {},
  i
}) => {
  const { name = 0, value = 0, flag = 0 } = data
  return (<tr>
    <td>{ccys[i]}</td>
    <td className="green" style={{ textAlign: 'right' }}>{name.toFixed(4)}</td>
    <td className="red">{value.toFixed(4)}</td>
    <td>{flag.toFixed(4)}</td>
    <td style={{ textAlign: 'right' }}>
      <img style={{ height: 20 }} src="/images/stock-chart.png" /></td>
  </tr>)
}
const List = ({ rows = new Array(12).fill({}) }) => {
  let data = (rows.length == 10)
    ? rows.concat([{}, {}])
    : rows
  return (<table className="c1list">
    <thead>
      <tr>
        <td>Ccy</td>
        <td style={{ textAlign: 'right' }}>Bid Price</td>
        <td>Ask Price</td>
        <td>Volume</td>
        <td></td>
      </tr>
    </thead>
    <tbody>
      {data.map((row, i) => (<Row key={i} data={row} i={i} />))}
    </tbody>
  </table>)
}

export default List