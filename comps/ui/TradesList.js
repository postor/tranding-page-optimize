const Row = ({
  data = {},
  i
}) => {
  const { name = 0, value = 0, flag = 0 } = data
  return (<tr>
    <td>{name.toFixed(4)}</td>
    <td className="green">{value.toFixed(4)}</td>
    <td>{flag.toFixed(4)}</td>
  </tr>)
}
const List = ({ rows = new Array(7).fill({}) }) => {
  let data = rows.length > 7 ? rows.slice(0, 7) : rows
  return (<table className="c1list">
    <thead>
      <tr>
        <td>Amount</td>
        <td>Price</td>
        <td>Time</td>
      </tr>
    </thead>
    <tbody>
      {data.map((row, i) => (<Row key={i} data={row} i={i} />))}
    </tbody>

  </table>)
}

export default List