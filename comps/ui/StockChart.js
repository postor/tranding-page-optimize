import { prepareCanvasJS } from '../../lib/data-chart-prepare'
import { useRef, useEffect } from 'react'
import Head from 'next/head'

const isServer = typeof window == 'undefined'

const MyLineChart = ({ data = [] }) => {
  let canvas = useRef(), chart = useRef(), data1 = useRef(), data2 = useRef()
  useEffect(() => {
    // init
    if (isServer) return

    Highcharts.getJSON('https://demo-live-data.highcharts.com/aapl-ohlcv.json', function (data) {

      // split the data set into ohlc and volume
      var ohlc = [],
        volume = [],
        dataLength = data.length,
        // set the allowed units for data grouping
        groupingUnits = [[
          'week',                         // unit name
          [1]                             // allowed multiples
        ], [
          'month',
          [1, 2, 3, 4, 6]
        ]],

        i = 0;

      for (i; i < dataLength; i += 1) {
        ohlc.push([
          data[i][0], // the date
          data[i][1], // open
          data[i][2], // high
          data[i][3], // low
          data[i][4] // close
        ]);

        volume.push([
          data[i][0], // the date
          data[i][5] // the volume
        ]);
      }
      data1.current = ohlc
      data2.current = volume

      // create the chart
      chart.current = Highcharts.stockChart(canvas.current, {

        rangeSelector: {
          selected: 1,
          buttons: [{
            type: 'month',
            count: 1,
            text: '5m'
          }, {
            type: 'month',
            count: 3,
            text: '15m'
          }, {
            type: 'month',
            count: 6,
            text: '30m'
          }, {
            type: 'ytd',
            text: '1h'
          }, {
            type: 'year',
            count: 1,
            text: '12h'
          }, {
            type: 'all',
            text: 'All'
          }]
        },
        navigator: {
          xAxis: {
            labels: {
              formatter: function () {
                let d = new Date(this.value)
                let h = d.getMonth(), m = Math.floor(d.getDate() / 30 * 60)
                h = (d.getUTCFullYear() % 2) ? h : h + 12
                return (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m)
              }
            },
          },
          series: {
            color: '#000'
          }
        },
        title: false,
        xAxis: {
          labels: {
            formatter: function () {
              let d = new Date(this.value)
              let h = d.getMonth(), m = Math.floor(d.getDate() / 30 * 60)
              h = (d.getUTCFullYear() % 2) ? h : h + 12
              return (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m)
            }
          },
        },
        yAxis: [{
          labels: {
            align: 'right',
            x: -3
          },
          title: {
            text: 'OHLC'
          },
          height: '60%',
          lineWidth: 2,
          resize: {
            enabled: true
          }
        }, {
          labels: {
            align: 'right',
            x: -3
          },
          title: {
            text: 'Volume'
          },
          top: '65%',
          height: '35%',
          offset: 0,
          lineWidth: 2
        }],

        tooltip: {
          split: true
        },

        series: [{
          type: 'candlestick',
          name: 'AAPL',
          data: ohlc,
          dataGrouping: {
            units: groupingUnits
          }
        }, {
          type: 'column',
          name: 'Volume',
          data: volume,
          yAxis: 1,
          dataGrouping: {
            units: groupingUnits
          }
        }]
      });
    });
  }, [])

  useEffect(() => {
    if (!chart.current) return
    data1.current = updateData(data1.current)
    data2.current = updateData(data2.current)
    chart.current.series[0].updateData(data1.current)
    chart.current.series[1].updateData(data2.current)
    chart.current.redraw()
  }, [data])

  return (<>
    <Head>
      <script key="highstock" src="https://code.highcharts.com/stock/highstock.js"></script>
      <script key="highstock-data" src="https://code.highcharts.com/stock/modules/data.js"></script>
      <script key="highstock-drag" src="https://code.highcharts.com/stock/modules/drag-panes.js"></script>
      <script key="highstock-exporting" src="https://code.highcharts.com/stock/modules/exporting.js"></script>
      <script key="highstock-unica" src="https://code.highcharts.com/themes/dark-unica.js"></script>
    </Head>
    <div ref={canvas} style={{
      height: '100%',
      width: '100%',
      position: 'relative',
      top: 0,
      left: 0,
    }} />
  </>)
}

export default MyLineChart

function updateData(data1 = []) {
  return data1.map(x => x.map((y, i) => i == 0 ? y : (y * (1 + Math.random()/10))))
}