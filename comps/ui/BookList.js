const bgspanStyle = {
  position: "absolute",
  height: '100%',
  zIndex: 0,
  opacity: 0.5
}

const spanStyle = {
  position: "relative",
  zIndex: 1,
}

const Row = ({
  data = {},
  i
}) => {
  const { name = 0, value = 0, flag = 0 } = data
  return (<tr>
    <td><span style={{
      ...bgspanStyle,
      backgroundColor: '#0f0',
      left: 0,
      width: (name * 100).toFixed(4) + '%'
    }} /><span style={spanStyle}>{name.toFixed(4)}</span></td>
    <td className="green" style={{
      textAlign: "right"
    }}>{value.toFixed(4)}</td>
    <td className="red">{value.toFixed(4)}</td>
    <td><span style={{
      ...bgspanStyle,
      backgroundColor: '#f00',
      right: 0,
      width: (flag * 100).toFixed(4) + '%'
    }} />
      <span style={spanStyle}>{flag.toFixed(4)}</span></td>
  </tr>)
}
const List = ({ rows = new Array(7).fill({}) }) => {
  let data = rows.length > 7 ? rows.slice(0, 7) : rows
  return (<table className="c1list">
    <thead>
      <tr>
        <td>Bid Amount</td>
        <td style={{ textAlign: "right" }}>Bid Price</td>
        <td>Ask Price</td>
        <td>Ask Amount</td>
      </tr>
    </thead>
    <tbody>
      {data.map((row, i) => (<Row key={i} data={row} i={i} />))}
    </tbody>

  </table>)
}

export default List