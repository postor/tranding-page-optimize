// import Head from 'next/head'

const Style = () => (<>
  <style jsx global>{`
  span {
    display: inline-block;
    width: 20px;
    white-space: nowrap;
    overflow: hidden;
  }
  .left {
    float: left;
  }
  `}</style>
</>)

export default Style