# 

## 安装运行

```
npm i
npm run dev
```

## 页面列表

- http://localhost:3000/index 综合3列表+2折线图
- http://localhost:3000/list/normal react常规列表
- http://localhost:3000/list/normal-batch react常规列表x50
- http://localhost:3000/list/ref 直接操作DOM更新列表
- http://localhost:3000/list/ref-batch 直接操作DOM更新列表x50
- http://localhost:3000/chart/rechart rechart实现图表
- http://localhost:3000/chart/canvasjs CanvasJS实现图表
- http://localhost:3000/chart/svg svg实现图表
