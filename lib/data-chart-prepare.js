
export function prepareRechart(data = []) {
  for (let i = 0; i < data.length; i++) {
    data[i].name = 'stock' + i
  }
  return data
}


export function prepareCanvasJS(data = []) {
  return data.map((y, x) => ({ y: y.value, x }))
}