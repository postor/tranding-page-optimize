const ROW_COUNT = 10, isServer = typeof window == 'undefined'

class DataSource {
  constructor() {
    this.counter = 0
    this.cbs = {
      data: [],
      start: [],
      stop: []
    }
    this.endTime = this.startTime = new Date()
    this.working = false
  }

  on(type, cb) {
    this.cbs[type].push(cb)
  }

  loop() {
    this.counter++
    let data = this.randomData()
    for (let cb of this.cbs.data) {
      cb(data)
    }
    if (this.working) setTimeout(() => this.loop())
    // if (this.working) Promise.resolve().then(() => this.loop())
  }

  start() {
    if (isServer) return
    this.counter = 0
    this.startTime = new Date()
    this.working = true
    this.loop()
    for (let cb of this.cbs.start) {
      cb()
    }
  }

  stop() {
    this.endTime = new Date()
    this.working = false
    for (let cb of this.cbs.stop) {
      cb()
    }
  }

  randomData() {
    let rtn = new Array(ROW_COUNT)
    for (let i = 0; i < ROW_COUNT; i++) {
      rtn[i] = {
        name: Math.random(),
        value: Math.random(),
        flag: Math.random(),
      }
    }
    return rtn
  }
}

let instance = new DataSource()

export default instance