import dataSource from './data-source'
import { useState, useEffect } from 'react'


/**
 * hooks for data
 */
export function useData() {
  let [data, setData] = useState([])
  useEffect(() => {
    dataSource.on('data', data => setData(data))
  }, [])
  return data
}

/**
 * hooks for working
 */
export function useWorking() {
  let [working, setWorking] = useState(false)
  let [speed, setSpeed] = useState(0)
  useEffect(() => {
    setWorking(dataSource.working)
    dataSource.on('start', _ => setWorking(true))
    dataSource.on('stop', _ => {
      setWorking(false)
      setSpeed((dataSource.endTime - dataSource.startTime) / dataSource.counter)
    })
  }, [])
  return [
    working,
    working => working
      ? dataSource.start()
      : dataSource.stop(),
    speed
  ]
}